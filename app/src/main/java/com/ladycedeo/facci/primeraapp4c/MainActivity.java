package com.ladycedeo.facci.primeraapp4c;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button ButtonLogin, ButtonGuardar, ButtonBuscar, btnParametro, btnfragmentos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButtonLogin =(Button) findViewById(R.id.btnLogin);
        ButtonGuardar = (Button)findViewById(R.id.Registrar);
        ButtonBuscar = (Button)findViewById(R.id.Buscar);

        btnParametro = (Button)findViewById(R.id.btnPasarParametro);
        btnfragmentos = (Button)findViewById(R.id.fragmentos);

        btnfragmentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,fragmentos.class);
                startActivity(intent);
            }
        });

        btnParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,PasarParametro.class);
                startActivity(intent);
            }
        });



        ButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Login.class);
                startActivity(intent);
            }

        });

        ButtonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,Registrar.class);
                startActivity(intent);
            }
        });

        ButtonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,Buscar.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(MainActivity.this);
                dialogoLogin.setContentView(R.layout.dlg_log);

                Button btnAutenticar = (Button) dialogoLogin.findViewById(R.id.btnAutenticar);
                final EditText cajaUsuario =(EditText) dialogoLogin.findViewById(R.id.txtUser);
                final EditText cajaClave =(EditText) dialogoLogin.findViewById(R.id.txtPassword);

                btnAutenticar.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {

                     Toast.makeText(MainActivity.this,cajaUsuario.getText().toString() + " " + cajaClave.getText().toString(),Toast.LENGTH_LONG).show();

                    }

                });





                dialogoLogin.show();
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionRegistrar:
                intent = new Intent(MainActivity.this, Registrar.class);
                startActivity(intent);
                break;
        }

        return true;
    }
}
